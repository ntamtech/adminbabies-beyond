
import Foundation
struct User_data : Codable {
	let id : Int?
	let name : String?
	let email : String?
	let phone : String?
	let photo : String?
	let notification_token : String?
	let birthday : String?
	let user_type_id : String?
	let created_by : String?
	let is_logged_in : String?
	let created_at : String?
	let updated_at : String?
	let user_token : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case email = "email"
		case phone = "phone"
		case photo = "photo"
		case notification_token = "notification_token"
		case birthday = "birthday"
		case user_type_id = "user_type_id"
		case created_by = "created_by"
		case is_logged_in = "is_logged_in"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case user_token = "user_token"
	}
}
