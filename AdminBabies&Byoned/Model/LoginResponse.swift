
import Foundation
struct LoginResponse : Codable {
	let status : Bool?
	let message : String?
	let data : LoginData?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data
	}

}
