
import Foundation
struct AllStaffWorkshopsData : Codable {
	let id : String?
	let name : String?
	let start_date : String?
	let end_date : String?
	let description : String?
	let speaker_name : String?
	let speaker_bio : String?
	let location : String?
	let price : String?
	let point : String?
	let created_by : String?
	let created_at : String?
	let updated_at : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case start_date = "start_date"
		case end_date = "end_date"
		case description = "description"
		case speaker_name = "speaker_name"
		case speaker_bio = "speaker_bio"
		case location = "location"
		case price = "price"
		case point = "point"
		case created_by = "created_by"
		case created_at = "created_at"
		case updated_at = "updated_at"
	}
}
