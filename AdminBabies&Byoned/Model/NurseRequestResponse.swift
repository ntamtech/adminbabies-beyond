
import Foundation
struct NurseRequestResponse : Codable {
	var status : Bool?
	var message : String?
	var data : NurseRequestData?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}
}
