
import Foundation
struct MidWifesTimes : Codable {
    var startHour:String?
    var endHour:String?
    var dayName:String?
    var date:String?
    
    enum CodingKeys: String, CodingKey {
        case startHour = "from"
        case endHour = "to"
        case dayName = "day"
        case date
    }
}
