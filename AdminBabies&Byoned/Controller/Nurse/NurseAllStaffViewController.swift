//
//  NurseAllStaffViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 1/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import ESPullToRefresh
import Toast_Swift

class NurseAllStaffViewController: UIViewController , AllStaffDelegate{
    
    // MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:- Variables
    var navBar:UINavigationController?
    let cellIdentifier = "NurseAllStaffTableViewCell"
    var allstaffnurserequests = [All_staff]()
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        let nib = UINib.init(nibName: "NurseAllStaffTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier:cellIdentifier)
        observeChanges()

        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self?.isRefresh = true
                    self?.showStaffRequests()
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.tabBarController?.view.makeToast("Check your Internet Connection!")
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }
    }

    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }

    override func viewWillAppear(_ animated: Bool) {
        setUpView()
    }

    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    //func if internet connection lost suddenly and to update data if internet come sudden
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
   
    func updateUI(){
        if AccountManager.shared().nurseRequestRespone?.data?.all_staff == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
                
            }
        }
    }
    
    //file for reload data
    fileprivate func setUpView() {
        if let allStaffRequests = AccountManager.shared().nurseRequestRespone?.data?.all_staff{
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            self.allstaffnurserequests = allStaffRequests
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            showStaffRequests()
            
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }
    
    func showAllStaffNurseDetails(btnTag: Int) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NurseAllStaffDetailsViewController") as! NurseAllStaffDetailsViewController
        vc.allStaffData = allstaffnurserequests[btnTag]
        self.navBar?.pushViewController(vc, animated: true)
    }
    
    func showStaffRequests(){
        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }
        
        let serviceTypeId = Constants.NurseTypeID
        
        let parameters = ["admin_id":adminID,"service_type_id":serviceTypeId] as [String:Any]
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        AdminServices.allAdminNurseRequests(Constants.AllAdminNurseRequestApi, params: parameters, completion: { (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false
                    let result = UIScreen.main.bounds.size
                    if result.height <= 568{
                        self.connectionBtn?.setTitle(Constants.FailureAlertiPhone5, for: .normal)
                    }else{
                        self.connectionBtn?.setTitle(Constants.FailureAlert, for: .normal)
                    }
                    
                }else{
                    self.tableView.es.stopPullToRefresh()
                    self.tabBarController?.view.makeToast("Check your Internet Connection!")
                }
            }else{
                // response
                if let result = response as? NurseRequestResponse {
                    AccountManager.shared().nurseRequestRespone = result
                    self.connectionBtn?.isHidden = true
                    self.tableView?.isHidden = false
                    self.allstaffnurserequests.removeAll()
                    self.isRefresh = false
                    self.allstaffnurserequests = (AccountManager.shared().nurseRequestRespone?.data?.all_staff)!
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()

                }
                
            }
        })
        
        
    }
    
}


extension NurseAllStaffViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allstaffnurserequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)  as! NurseAllStaffTableViewCell
        let obj = allstaffnurserequests[indexPath.row]
        cell.allStaffViewDetails.tag = indexPath.row
        if let name = obj.name{
            cell.allStaffName.text = name
        }
        let reservedstatus = obj.is_reserved
            if reservedstatus == true{
                cell.allStaffReserved.text = "Reserved"
            }
            else{
                cell.allStaffReserved.text = "Not Reserved"

            }
        
        if obj.photo != nil {
           cell.allStaffImage.sd_setImage(with: URL(string : obj.photo!), placeholderImage: UIImage(named: "Personimage"))
        }
        cell.delegate = self
        return cell
    }

    
}

