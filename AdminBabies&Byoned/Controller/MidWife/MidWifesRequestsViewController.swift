//
//  MidWifesRequestsViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/28/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidWifesRequestsViewController: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:- Variables
    var navBar:UINavigationController?
    let cellIdentifier = "RequestsMidWifesCell"
    var RequestsMidWifesStaff = [MidWifesReservedData]()
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        let nib = UINib.init(nibName: "RequestsMidWifesCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier:cellIdentifier)
        observeChanges()
        
        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self?.isRefresh = true
                    self?.showReservationsRequests()
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.tabBarController?.view.makeToast("Check your Internet Connection!")
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }
        
        
    }
    
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Hide the navigation bar for current view controller
        self.navigationController?.isNavigationBarHidden = true
        setUpView()
    }
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(midwifeChanged(_:)), name: NSNotification.Name(rawValue: NotificationType.Midwife.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Midwife.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    @objc func midwifeChanged(_ notification: NSNotification) {
        guard let action = notification.userInfo?[Constants.Action] as? String else{
            return
        }
        switch action {
        case NotificationAction.CancelAction.rawValue:
            guard let midwifeId = notification.userInfo?[Constants.Id] as? String else {
                return
            }
            if let i = AccountManager.shared().midwifesReservedRequestResponse?.data?.index(where:{$0.id == midwifeId}) {
                AccountManager.shared().midwifesReservedRequestResponse?.data?.remove(at: i)
                RequestsMidWifesStaff.remove(at: i)
            }
            self.tableView.reloadData()
            break
        case NotificationAction.AddAction.rawValue:
            if let midwifeObj = notification.userInfo?[Constants.serviceObj] as? [String:Any] {
                let jsonData = try? JSONSerialization.data(withJSONObject: midwifeObj, options: [])
                var midWifeData: MidWifesReservedData?
                if let data = jsonData {
                    midWifeData = try? JSONDecoder().decode(MidWifesReservedData.self, from: data)
                }
                AccountManager.shared().midwifesReservedRequestResponse?.data?.append(midWifeData!)
                RequestsMidWifesStaff.append(midWifeData!)

                self.tableView.reloadData()
            }
            break
        default: break
        }
    }
    
    func updateUI() {
        if AccountManager.shared().midwifesReservedRequestResponse == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
            }
        }
    }
    fileprivate func setUpView() {
        if let workshops = AccountManager.shared().midwifesReservedRequestResponse{
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            showReservationsRequests()
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }
    
    
    func showReservationsRequests(){
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        AdminServices.getMidewifeReservations(Constants.MidwifeReservationsApi, completion: { (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false

                    let result = UIScreen.main.bounds.size
                    if result.height <= 568{
                        self.connectionBtn?.setTitle(Constants.FailureAlertiPhone5, for: .normal)
                    }else{
                        self.connectionBtn?.setTitle(Constants.FailureAlert, for: .normal)
                    }
                }else{                    
                    self.tableView.es.stopPullToRefresh()
                    self.tabBarController?.view.makeToast("Check your Internet Connection!")
                    
                }
            }else{
                // response
                if let result =  response as? MidwifesReservedResponse {
                    AccountManager.shared().midwifesReservedRequestResponse = result
                    self.RequestsMidWifesStaff.removeAll()
                    self.RequestsMidWifesStaff = (AccountManager.shared().midwifesReservedRequestResponse?.data)!
                    self.connectionBtn?.isHidden = true
                    self.tableView?.isHidden = false
                    self.isRefresh = false
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()
                }else{
                    return
                }
                
            }
        })
        
        
    }
    
    
}

extension MidWifesRequestsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RequestsMidWifesStaff.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)  as! RequestsMidWifesCell
        let obj = RequestsMidWifesStaff[indexPath.row]
        if let name = obj.user_name{
            cell.RequestMidWifesName.text = name
        }
        if let image = obj.photo{
            cell.RequestMidWifesImage.sd_setImage(with: URL(string: obj.photo!), placeholderImage: UIImage(named: "Personimage"))
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MidWifesAssigmentViewController") as! MidWifesAssigmentViewController
        vc.selectedMidwife = RequestsMidWifesStaff[indexPath.row]
        vc.midWifesRequestsViewController = self
        self.navBar?.pushViewController(vc, animated: true)

    }
}
