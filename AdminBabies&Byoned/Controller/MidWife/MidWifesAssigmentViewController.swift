//
//  MidWifesAssigmentViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/27/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidWifesAssigmentViewController: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var availableTimeSlotsTableView:UITableView!
    @IBOutlet weak var midwifeNameLabel:UILabel!
    @IBOutlet weak var midwifeImageView:UIImageView!
    @IBOutlet weak var noAvailabelTimesLabel: UILabel!
    @IBOutlet weak var addRequetBtn: UIButton!
    @IBOutlet weak var midwifeApproved: EMSpinnerButton!
    
    // MARK: private variables
    fileprivate let cellId = "timeSlotCell"
    fileprivate var days:[String:[ReservedTimeSlots]]?
    
    // MARK: public variables
    public var selectedMidwife:MidWifesReservedData?
    var midWifesRequestsViewController : MidWifesRequestsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.midwifeApproved.cornerRadius = 5.0
        self.midwifeApproved.backgroundColor = UIColor(red: 226/255, green: 202/255, blue: 193/255, alpha: 1.0)
        midwifeImageView.layer.cornerRadius = midwifeImageView.frame.size.height/2
        midwifeImageView.clipsToBounds = true
        self.title = "MidWife Requests"
        let attributes = [NSAttributedStringKey.font : UIFont(name: "MuseoSans-300", size: 20)!, NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        availableTimeSlotsTableView.register(UINib(nibName: "MidwifeTimeSlotTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        if let availabelTimeSlotsArr = selectedMidwife?.time_slots {
            if availabelTimeSlotsArr.count > 0 {
                days = groupTimeSlotsPerDay(availableTimes: availabelTimeSlotsArr)
                self.availableTimeSlotsTableView.isHidden = false
                self.noAvailabelTimesLabel.isHidden = true
                self.addRequetBtn.isEnabled = true
            }else{
                self.availableTimeSlotsTableView.isHidden = true
                self.noAvailabelTimesLabel.isHidden = false
                self.addRequetBtn.isEnabled = false
            }
        }else{
            self.availableTimeSlotsTableView.isHidden = true
            self.noAvailabelTimesLabel.isHidden = false
            self.addRequetBtn.isEnabled = false
        }
        if let midwife = selectedMidwife{
            midwifeNameLabel.text = midwife.name
        }
        if let midwifeimage = selectedMidwife{
            midwifeImageView.sd_setImage(with: URL(string: (selectedMidwife?.photo)!), placeholderImage: UIImage(named: "Personimage"))
        }
        
    }
    
    private func groupTimeSlotsPerDay(availableTimes : [ReservedTimeSlots]) -> [String:[ReservedTimeSlots]] {
        let groupedDays = Dictionary(grouping: availableTimes) { (availableTime) -> String in
            return availableTime.date!
        }
        return groupedDays
    }
    
   
    @IBAction func backBarButtonTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addMidwifeRequest(_ sender: Any) {
                if InternetConnection.connected(){
                    self.midwifeApproved.animate(animation: .collapse)
                    sendMidwifeRequest()

                }
        
                else{
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
                }
    }
    
    
    func sendMidwifeRequest(){
        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }
        
        guard let uniqueID = selectedMidwife?.id else{
            return
        }
        
        let parameters = ["admin_id":adminID,"unique_key":uniqueID] as [String:Any]
        AdminServices.MidwifeConfirmWithoutPayment(Constants.MidwifeConfirmWithoutPaymentApi, params: parameters, completion: { (response, error) in
            if let _ = error {
                self.midwifeApproved.animate(animation: .expand)
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let result =  response as? ConfirmWithoutPaymentResponse {
                    if result.status!{
                        self.midWifesRequestsViewController?.showReservationsRequests()
                        self.navigationController?.popViewController(animated: true)
                        self.midwifeApproved.animate(animation: .expand)

                    }else{
                        self.midwifeApproved.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: result.message!, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    
                    return
                }
            }
        })
    }
    
}
    


extension MidWifesAssigmentViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let allDays = days {
            return allDays.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let allDays = days {
            let dayNameKey = allDays.keys.sorted()[section]
            if let midwifeTimes = allDays[dayNameKey] {
                return midwifeTimes.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! MidwifeTimeSlotTableViewCell
        let dayNameKey = days!.keys.sorted()[indexPath.section]
        if let midwifeTimes = days![dayNameKey] {
            let endHourAsStringArr = midwifeTimes[indexPath.row].endHour!.components(separatedBy: ":")
            let endHourString = "\(endHourAsStringArr[0]):\(endHourAsStringArr[1])"
            let startHourAsStringArr = midwifeTimes[indexPath.row].startHour!.components(separatedBy: ":")
            let startHourString = "\(startHourAsStringArr[0]):\(startHourAsStringArr[1])"
            cell.endTimeLabel.text = " To \(endHourString)"
            cell.startTimeLabel.text = "From \(startHourString)"
        }
        cell.selectionStyle = .none
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let allDays = days {
            return allDays.keys.sorted()[section]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let screenWidth = UIScreen.main.bounds.size.width
        let headerFrame = CGRect(x: 0, y: 0, width: screenWidth, height: 40)
        let headerView = UIView(frame: headerFrame)
        headerView.backgroundColor = UIColor(r: 82, g: 87, b: 106)
        
        let dayNameFrame = CGRect(x: 10, y: 0, width: screenWidth/2, height: 40)
        let dayNameLabel = UILabel(frame: dayNameFrame)
        let font = UIFont(name: "MuseoSans-300", size: 17)
        dayNameLabel.font = font
        dayNameLabel.textColor = UIColor.white
        
        let dateLblFrame = CGRect(x: screenWidth/2 , y: 0, width: screenWidth/2 - 10, height: 40)
        let dateLabel = UILabel(frame: dateLblFrame)
        dateLabel.textAlignment = .right
        dateLabel.font = font
        dateLabel.textColor = UIColor.white        
        headerView.addSubview(dayNameLabel)
        headerView.addSubview(dateLabel)

        if let allDays = days {
            dateLabel.text = allDays.keys.sorted()[section]
            dayNameLabel.text = convertDateToDay(date:  dateLabel.text!)
        }
        
        return headerView
    }
    
    private func convertDateToDay(date:String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let date = formatter.date(from: date){
            formatter.dateFormat  = "EEEE"
            let dayName = formatter.string(from: date)
            return dayName
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
