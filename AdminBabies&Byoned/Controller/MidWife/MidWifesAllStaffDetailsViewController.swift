//
//  MidWifesAllStaffDetailsViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/27/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidWifesAllStaffDetailsViewController: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var availableTimeSlotsTableView:UITableView!
    @IBOutlet weak var midwifeNameLabel:UILabel!
    @IBOutlet weak var midwifeImageView:UIImageView!
    @IBOutlet weak var noAvailabelTimesLabel: UILabel!
    
    // MARK: private variables
    fileprivate let cellId = "timeSlotCell"
    fileprivate var days:[String:[MidWifesTimes]]?
    
    // MARK: public variables
    public var selectedMidwife:MidWifesData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MidWife Information"
        let attributes = [NSAttributedStringKey.font : UIFont(name: "MuseoSans-300", size: 20)!, NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        midwifeImageView.layer.cornerRadius = midwifeImageView.frame.size.height/2
        midwifeImageView.clipsToBounds = true
        availableTimeSlotsTableView.register(UINib(nibName: "MidwifeTimeSlotTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
        if let availabelTimeSlotsArr = selectedMidwife?.midwife_times {
            if availabelTimeSlotsArr.count > 0 {
                days = groupTimeSlotsPerDay(availableTimes: availabelTimeSlotsArr)
                self.availableTimeSlotsTableView.isHidden = false
                self.noAvailabelTimesLabel.isHidden = true
            }else{
                self.availableTimeSlotsTableView.isHidden = true
                self.noAvailabelTimesLabel.isHidden = false
            }
        }else{
            self.availableTimeSlotsTableView.isHidden = true
            self.noAvailabelTimesLabel.isHidden = false
        }
        if let midwife = selectedMidwife{
            midwifeNameLabel.text = midwife.name
        }
        if let midwifeimage = selectedMidwife{
            midwifeImageView.sd_setImage(with: URL(string: (selectedMidwife?.photo)!), placeholderImage: UIImage(named: "Personimage"))
        }
    }
    
    private func groupTimeSlotsPerDay(availableTimes : [MidWifesTimes]) -> [String:[MidWifesTimes]] {
        let groupedDays = Dictionary(grouping: availableTimes) { (availableTime) -> String in
            return availableTime.dayName!
        }
        return groupedDays
    }
    
    @IBAction func backBarButtonTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension MidWifesAllStaffDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let allDays = days {
            return allDays.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let allDays = days {
            let dayNameKey = allDays.keys.sorted()[section]
            if let midwifeTimes = allDays[dayNameKey] {
                return midwifeTimes.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! MidwifeTimeSlotTableViewCell
        let dayNameKey = days!.keys.sorted()[indexPath.section]
        if let midwifeTimes = days![dayNameKey] {
            let endHourAsStringArr = midwifeTimes[indexPath.row].endHour!.components(separatedBy: ":")
            let endHourString = "\(endHourAsStringArr[0]):\(endHourAsStringArr[1])"
            let startHourAsStringArr = midwifeTimes[indexPath.row].startHour!.components(separatedBy: ":")
            let startHourString = "\(startHourAsStringArr[0]):\(startHourAsStringArr[1])"
            cell.endTimeLabel.text = " To \(endHourString)"
            cell.startTimeLabel.text = "From \(startHourString)"
        }
        cell.selectionStyle = .none
        tableView.tableFooterView = UIView()
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let allDays = days {
            return allDays.keys.sorted()[section]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let screenWidth = UIScreen.main.bounds.size.width
        let headerFrame = CGRect(x: 0, y: 0, width: screenWidth, height: 40)
        let headerView = UIView(frame: headerFrame)
        headerView.backgroundColor = UIColor(r: 82, g: 87, b: 106)
        
        let dayNameFrame = CGRect(x: 10, y: 0, width: screenWidth-10, height: 40)
        let dayNameLabel = UILabel(frame: dayNameFrame)
        let font = UIFont(name: "MuseoSans-300", size: 17)
        dayNameLabel.font = font
        dayNameLabel.textColor = UIColor.white
        
        headerView.addSubview(dayNameLabel)
        if let allDays = days {
            dayNameLabel.text = allDays.keys.sorted()[section]
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
