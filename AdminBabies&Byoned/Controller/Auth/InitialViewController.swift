//
//  InitialViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let userData = Helper.getCachedUser() {
                AccountManager.shared().userData = userData
                let vc = storyboard.instantiateViewController(withIdentifier: "navigationController") as! UINavigationController
                self.present(vc, animated: true, completion: nil)
            }else{
                let signinVC = storyboard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                self.present(signinVC, animated: true, completion: nil)
            }
        }
    }
}
