//
//  CalenadarViewController.swift
//  Babies&Beyond
//
//  Created by esam ahmed eisa on 12/22/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import JBDatePicker

class CalenadarViewController: UIViewController {
    var occupiedNursesArr = [StaffDataByDate]()
    var delegate:CalenadarViewControllerDelegate?
    
    @IBOutlet weak var datePickerView: JBDatePickerView!
    var dateToSelect: Date!
    @IBOutlet weak var monthNameLbl: UILabel!
    var datePicker: JBDatePickerView!
    lazy var dateFormatter: DateFormatter = {
        var formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false;

        let frameForDatePicker = CGRect(x: 0, y: 20, width: view.bounds.width, height: 250)
        datePicker = JBDatePickerView(frame: frameForDatePicker)
        
        datePickerView.delegate = self
        
    }
//    override func viewDidAppear(_ animated: Bool) {
//        let frameForDatePicker = CGRect(x: 0, y: 20, width: view.bounds.width, height: 250)
//        datePicker = JBDatePickerView(frame: frameForDatePicker)
//
//        datePickerView.delegate = self
//    }
    @IBAction func loadNextMonth(_ sender: UIButton) {
        datePickerView.loadNextView()
    }
    
    @IBAction func loadPreviousMonth(_ sender: UIButton) {
        datePickerView.loadPreviousView()
    }
}
extension CalenadarViewController:  JBDatePickerViewDelegate  {
    func didSelectDay(_ dayView: JBDatePickerDayView) {
        print(dateFormatter.string(from: dayView.date!))
        var selectedDate = dateFormatter.string(from: dayView.date!)
        dayTapped(selectedDate:selectedDate)
    }

    func dayTapped(selectedDate:String) {
        if InternetConnection.connected() {
            getNurses(selectedDate: selectedDate)
            print("internet connected")
            
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    
    func getNurses(selectedDate:String){
        self.view.isUserInteractionEnabled = false
        self.delegate?.showLoading(isShow: true)
        let parameters = ["date" : selectedDate] as [String : Any]
        AdminServices.GetAvailableNurses(Constants.OccupiedNursesApi, params: parameters, completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            if let _ = error {
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // show alert with success request.
                if let occupiedNurses =  response as? StaffDataByDateResponse{
                    if occupiedNurses.status! {
                        if let nursesData = occupiedNurses.data{
                            self.occupiedNursesArr.removeAll()
                            self.occupiedNursesArr = nursesData
                             self.delegate?.showAllNursesData(staffDataByDate: self.occupiedNursesArr)
                        }
                    }
                    else{
                        Helper.showAlert(title: Constants.errorAlertTitle, message: (error?.localizedDescription)!, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
            }
        })
        
    }
    
    func didPresentOtherMonth(_ monthView: JBDatePickerMonthView) {
        monthNameLbl.text = datePickerView.presentedMonthView.monthDescription
    }
    
    var dateToShow: Date {
        if let date = dateToSelect {
            print(date)
            return date
        }else{
            return Date()
        }
    }
    
    var weekDaysViewHeightRatio: CGFloat {
        return 0.1
    }
    var firstWeekDay: JBWeekDay {
        return .monday
        
    }
    
    //custom colors
    var colorForWeekDaysViewBackground: UIColor {
        return UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    var colorForSelectionCircleForOtherDate: UIColor {
        return UIColor(red: 82.0/255.0, green: 87.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        
    }
    
    var colorForSelectionCircleForToday: UIColor {
        return UIColor(red: 82.0/255.0, green: 87.0/255.0, blue: 106.0/255.0, alpha: 1.0)
    }
    
    var colorForWeekDaysViewText : UIColor {
        return UIColor(red: 82.0/255.0, green: 87.0/255.0, blue: 106.0/255.0, alpha: 1.0)
        
    }
    var colorForCurrentDay: UIColor {
        return UIColor(red: 82.0/255.0, green: 87.0/255.0, blue: 106.0/255.0, alpha: 1.0)
    }
    
    var fontForWeekDaysViewText: JBFont {
        return JBFont(name: "MuseoSans-300", size: .large)
    }
    
    var fontForDayLabel: JBFont {
        return JBFont(name: "MuseoSans-300", size: .medium)
    }


}
protocol CalenadarViewControllerDelegate{
    func showAllNursesData(staffDataByDate : [StaffDataByDate])
    func showLoading(isShow : Bool)
}
