//
//  AddNewClientViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM on 3/29/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit


class AddNewClientViewController: UIViewController {

    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var birthdateTxt: UITextField!
    @IBOutlet weak var createClientBtn: EMSpinnerButton!
    @IBOutlet weak var imageView: UIImageView!
    
    var base64String:String?
    var allClientsViewController = AllClientsViewController()
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow icon"), style: UIBarButtonItemStyle.done, target: self, action: #selector(AddNewClientViewController.cancel(_:)))
    }
    
    @IBAction func cancel(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "New Client"
        imageView.layer.cornerRadius = imageView.frame.size.height/2
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        
        self.createClientBtn.cornerRadius = 5.0
        self.createClientBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.createClientBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        birthdateTxt.inputView = datePickerView
        
        var components = DateComponents()
        components.year = -100
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        
        components.year = -13
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        datePickerView.minimumDate = minDate
        datePickerView.maximumDate = maxDate
        
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        
    }
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        birthdateTxt.text = dateFormatter.string(from: sender.date)
    }
    @IBAction func uploadPhotoBtnTapped(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    @IBAction func createClientBtnTapped(_ sender: Any) {
        
        guard let name = nameTxt.text, !name.isEmpty else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyName, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let  phone = phoneTxt.text, !phone.isEmpty  else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyPhoneNumber, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
         
        guard let  email = emailTxt.text, !email.isEmpty  else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyEmail, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard  Validation.isValidEmail(email: email) else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.InvalidEmail, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let  password = passwordTxt.text, !password.isEmpty  else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyPassword, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let  confirmPassword = confirmPasswordTxt.text, !confirmPassword.isEmpty  else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.PasswordsNotMatched, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        if password != confirmPassword{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.PasswordsNotMatched, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let  birthday = birthdateTxt.text, !birthday.isEmpty  else {
            return
        }

        if InternetConnection.connected(){
            self.createClientBtn.animate(animation: .collapse)
            createClient(name: name, phone: phone, email: email, password:password, birthday:birthday )
         }else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
         }
        
        
        
    }
    
    func createClient (name: String, phone: String, email: String, password:String, birthday:String){
        var parameters = ["name":name,"email":email,"phone": phone,"birthday":birthday,"password": password] as [String:Any]
        
        
        if let photo = base64String{
            parameters["photo"] = photo
        }
        
        AdminServices.createClient(Constants.CreateClientApi, params: parameters, completion: { (response, error) in
            if let _ = error {
                self.createClientBtn.animate(animation: .expand)
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                //response
                if let result =  response as? CreateClientResponse {
                    if result.status!{
                        self.allClientsViewController.clientData.append(result.data!)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.createClientBtn.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    return
                }
            }
        })
    }
   
    
    
}
extension AddNewClientViewController: UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        var selectedImageFromPicker: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
            selectedImageFromPicker = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        dismiss(animated: true, completion: nil)
        if let selectedImage = selectedImageFromPicker
        {
            imageView.image = selectedImage
            let imageData = UIImagePNGRepresentation(imageView.image!)
            base64String = imageData?.base64EncodedString(options: .endLineWithLineFeed)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

