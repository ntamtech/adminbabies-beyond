//
//  MainTabBarViewController.swift
//  Babies&Beyond
//
//  Created by NTAM on 1/17/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

//import Foundation
import UIKit

class MainTabBarViewController: UITabBarController,UINavigationControllerDelegate {
    let cellID = "MoreTableViewCell"
    var tableview:UITableView?
    var itemIconArr = ["account-search1","calendar1","clientIcon","accountIcon"]
    var itemNameArr = ["Available Nurse","Nurse Calendar","Clients","Account"]
    
    override func viewDidLoad() {
        delegate = self
//        MainTabBarViewController.customizableViewControllers = nil;
       // MainTabBarViewController
        customizableViewControllers = nil
    }
}
extension MainTabBarViewController: UITabBarControllerDelegate, UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemNameArr.count
    }
    

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

        if (viewController == tabBarController.moreNavigationController && tabBarController.moreNavigationController.delegate == nil) {
            tableview = self.moreNavigationController.topViewController?.view as? UITableView
            tableview?.register(UINib(nibName: "MoreTableViewCell", bundle: nil), forCellReuseIdentifier: cellID)
            //tableview?.separatorInset = UIEdgeInsets.zero
            tableview?.delegate = self
            tableview?.dataSource = self
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = self.tableview?.dequeueReusableCell(withIdentifier: "MoreTableViewCell") as! MoreTableViewCell
        cell.itemIcon.image = UIImage(named: itemIconArr[indexPath.row])
        cell.itemName.text = itemNameArr[indexPath.row]
        tableview?.tableFooterView = UIView()
        cell.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        switch row {
        case 0:
            let VC = storyboard?.instantiateViewController(withIdentifier: "AvailableNurseViewController") as! AvailableNurseViewController
            self.navigationController?.pushViewController(VC, animated: true)
            break
            
        case 1:
            let VC = storyboard?.instantiateViewController(withIdentifier: "NurseCalendarViewController") as! NurseCalendarViewController
            self.navigationController?.pushViewController(VC, animated: true)
            break
        case 2:
            let VC = storyboard?.instantiateViewController(withIdentifier: "AllClientsViewController") as! AllClientsViewController

            self.navigationController?.pushViewController(VC, animated: true)
            break

        case 3:
            
            let VC = storyboard?.instantiateViewController(withIdentifier: "LogoutViewController") as! LogoutViewController
            self.navigationController?.pushViewController(VC, animated: true)
            break
        default:
            break
        
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

}
