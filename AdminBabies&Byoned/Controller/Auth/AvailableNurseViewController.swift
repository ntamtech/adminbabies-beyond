//
//  AvailableNurseViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 4/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import STPopup


class AvailableNurseViewController: UIViewController {

    @IBOutlet weak var startDateBtn: UIButton!
    @IBOutlet weak var endDateBtn: UIButton!
    @IBOutlet weak var searchBtn: EMSpinnerButton!

    var isNavigationBarHidden:Bool = true
    var minimumEndDate : Date?
    var selectedStartDateTime : String?
    var selectedEndDateTime : String?
    var availableNurseArray = [StaffDataByDate]()
    var searchDetails:SearchAvailableNurseDetailsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.searchBtn.cornerRadius = 5.0
        self.searchBtn.backgroundColor = UIColor(r: 226, g: 202, b: 193)
        self.searchBtn.titleColor = UIColor(r: 82, g: 87, b: 106)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Available Nurse"
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow icon"), style: UIBarButtonItemStyle.done, target: self, action: #selector(AvailableNurseViewController.cancel(_:)))
        if let selectedStartDateTime = selectedStartDateTime{
            startDateBtn.setTitle(selectedStartDateTime, for: .normal)
        }
        if let selectedEndDateTime = selectedEndDateTime{
            endDateBtn.setTitle(selectedEndDateTime, for: .normal)
        }
    }
    
    @IBAction func cancel(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    
    @IBAction func chooseStartDate(_ sender: Any) {
            isNavigationBarHidden = false
            let destination = self.storyboard!.instantiateViewController(withIdentifier: "DatePopupPickerViewController") as? DatePopupPickerViewController
            destination?.availableNurseVC = self
            destination?.isStartDate = true
            destination?.contentSizeInPopup = CGSize(width: 300, height: 300)
            let popupController = STPopupController.init(rootViewController: destination!)
            popupController.navigationBarHidden = true
            popupController.present(in: self)
    }
    
    
    @IBAction func chooseEndDate(_ sender: Any) {
        if minimumEndDate == nil{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.StartDateAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
        else{
            isNavigationBarHidden = false
           let destination = self.storyboard!.instantiateViewController(withIdentifier: "DatePopupPickerViewController") as? DatePopupPickerViewController
            destination?.availableNurseVC = self
            destination?.isStartDate = false
            destination?.contentSizeInPopup = CGSize(width: 300, height: 300)
            let popupController = STPopupController.init(rootViewController: destination!)
            popupController.navigationBarHidden = true
            popupController.present(in: self)
        }
    }
    
    @IBAction func searchBtnTapped(_ sender: Any) {
        guard let startDateTime = selectedStartDateTime, !startDateTime.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.StartDateAlert, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        guard let endDateTime = selectedEndDateTime, !endDateTime.isEmpty else {
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EndDateAlert, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        if InternetConnection.connected() {
          SearchForNursesAvailable(startDateTime: startDateTime, endDateTime: endDateTime)
            print("internet connected")
           
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    
    func SearchForNursesAvailable(startDateTime:String,endDateTime:String){
//        guard let adminID = AccountManager.shared().userData?.id else{
//            return
//        }
        self.view.isUserInteractionEnabled = false
        self.searchBtn.animate(animation: .collapse)
        let parameters = ["start_date" : startDateTime, "end_date": endDateTime] as [String : Any]
        AdminServices.GetAvailableNurses(Constants.AvailableNursesApi, params: parameters, completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            self.searchBtn.animate(animation: .expand)
            if let _ = error {
                self.searchBtn.animate(animation: .expand)
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // show alert with success request.
                if let availableNurse =  response as? StaffDataByDateResponse{
                    if availableNurse.status! {
                        self.searchBtn.animate(animation: .collapse)
                        //AccountManager.shared().availableNurse?.data
                        self.availableNurseArray = availableNurse.data!
                        let mainstoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = mainstoryboard.instantiateViewController(withIdentifier: "SearchAvailableNurseDetailsViewController") as! SearchAvailableNurseDetailsViewController
                            vc.availableNursesSearch = self.availableNurseArray
                            self.searchBtn.animate(animation: .expand)
                            self.navigationController?.pushViewController(vc, animated: true)
//                        self.startDateBtn.setTitle("", for: .normal)
//                        self.endDateBtn.setTitle("", for: .normal)
                    }
                    else{
                        self.searchBtn.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: (error?.localizedDescription)!, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
            }
        })
        
}

}
