//
//  NurseCalendarViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 4/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import ISHPullUp
class NurseCalendarViewController:  ISHPullUpViewController  {

//    var calenadarViewController: CalenadarViewController?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        commonInit()
    }
    
    private func commonInit() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let calendarVC = storyBoard.instantiateViewController(withIdentifier: "content") as! CalenadarViewController
        let bottomVC = storyBoard.instantiateViewController(withIdentifier: "bottom") as! ScheduleViewController
        calendarVC.delegate = bottomVC
        contentViewController = calendarVC
        bottomViewController = bottomVC
        bottomVC.pullUpController = self
        contentDelegate = calendarVC as? ISHPullUpContentDelegate
        sizingDelegate = bottomVC as? ISHPullUpSizingDelegate
        stateDelegate = bottomVC as? ISHPullUpStateDelegate
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Nurse Calendar"
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow icon"), style: UIBarButtonItemStyle.done, target: self, action: #selector(NurseCalendarViewController.cancel(_:)))
    }
    
    @IBAction func cancel(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

}
