//
//  SignInViewController.swift
//  AdminBabies&Byoned
//
//  Created by esam ahmed eisa on 1/1/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import PKHUD

class SignInViewController: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var logintapped: EMSpinnerButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.logintapped.cornerRadius = 5.0
       self.logintapped.backgroundColor = UIColor(red: 226/255, green: 202/255, blue: 193/255, alpha: 1.0)
    }
    override func viewWillAppear(_ animated: Bool) {
        // Hide the navigation bar for current view controller
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    
    @IBAction func signInPressAction(_ sender: Any) {
        guard let email = emailText.text, !email.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyEmail, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard  Validation.isValidEmail(email: email) else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.InvalidEmail, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let password = passwordText.text, !password.isEmpty else {
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.EmptyPassword, controller: self, okBtnTitle: Constants.OKTItle)
            return
        }
        
        guard let notificationToken = defaults.string(forKey: "notificationToken"), !notificationToken.isEmpty else {
            return
        }
        
        
        
        if InternetConnection.connected() {
            self.view.isUserInteractionEnabled = false
            self.logintapped.animate(animation: .collapse)
            LoginRequest(email: email, password: password, notificationToken : notificationToken )
            
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
        
        
    }
    
    
    
        
    func LoginRequest(email:String,password:String, notificationToken:String) {
        let parameters = ["email":email, "password":password, "notification_token":notificationToken] as [String:Any]
        AdminServices.AdminLoginService(Constants.LoginApi, params: parameters , completion: { (response, error) in
            self.view.isUserInteractionEnabled = true
            if let error = error {
                self.logintapped.animate(animation: .expand)
                // show alert with error message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // if the response isn't nil
                if let loginResponseResult = response as? LoginResponse {
                    // if status is true
                    if let status = loginResponseResult.status {
                        if status {
                            if let userData = loginResponseResult.data?.user_data {
                                AccountManager.shared().userData = userData
                                Helper.cacheUser(userData: userData)
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "navigationController") as! UINavigationController
                                self.present(vc, animated: true, completion: nil)
                                return
                            }
                        }
                    }
                    // if status is false
                    self.logintapped.animate(animation: .expand)
                    Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.NoAdmin, controller: self, okBtnTitle: Constants.OKTItle)
                }else{
                    // if the response is nil
                    self.logintapped.animate(animation: .collapse)
                }
            }
        })
   }
}
    

