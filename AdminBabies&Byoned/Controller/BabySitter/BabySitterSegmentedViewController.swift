//
//  BabySitterSegmentedViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 1/6/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class BabySitterSegmentedViewController: UIViewController ,UIPageViewControllerDataSource,UIPageViewControllerDelegate, UIScrollViewDelegate{

    // MARK:- Outlets
    @IBOutlet weak var babySitterSegmentedcontrol: UISegmentedControl!
    
    // MARK:- Variables
    var babySitterRequests:BabySitterRequestViewController! = nil
    var allStaff:BabySitterAllStaffVC!
    private var pageController:UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentpage:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        currentpage = 0
        babySitterSegmentedcontrol.selectedSegmentIndex = 0
        createPageViewController()
    }
    
    //MARK: - CreatePagination
    private func createPageViewController()
    {
        pageController = UIPageViewController.init(transitionStyle: UIPageViewControllerTransitionStyle.scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.horizontal, options: nil)
        
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        
        let mainstoryboard = UIStoryboard(name: "Main", bundle: nil)
        babySitterRequests = mainstoryboard.instantiateViewController(withIdentifier: "BabySitterRequestViewController") as! BabySitterRequestViewController
        babySitterRequests.navBar = self.navigationController
        allStaff = mainstoryboard.instantiateViewController(withIdentifier: "BabySitterAllStaffVC") as! BabySitterAllStaffVC
        allStaff.navBar = self.navigationController
        arrVC = [babySitterRequests,allStaff]
        // it's important method which detect all the view controllers that you paginate over them
        pageController.setViewControllers([babySitterRequests], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        self.view.addSubview(pageController.view)
        
    }
    
    override func viewDidLayoutSubviews() {
        if UIDevice().userInterfaceIdiom == .phone{
            var screenSize = UIScreen.main.bounds.size
            if (screenSize.height == 812){
                self.pageController.view.frame = CGRect(x: 0, y: 84, width: self.view.frame.size.width, height: self.view.frame.size.height-84)
            }else{
                self.pageController.view.frame = CGRect(x: 0, y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height-64)
            }
        }
    }
    //MARK: - Custom Methods
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.index(of: viewCOntroller)!
        }
        
        return -1
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (completed) {
            currentpage = arrVC.index(of: (pageViewController.viewControllers?.last)!)
            resetSegmenteControl(index: currentpage)
        }
    }
    
    //change segmentedControl
    private func resetSegmenteControl(index: Int) {
        babySitterSegmentedcontrol.selectedSegmentIndex = index
    }
    
    
    @IBAction func babySitterSegmentedAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            pageController.setViewControllers([arrVC[sender.selectedSegmentIndex]], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        }else if sender.selectedSegmentIndex == 1 {
            pageController.setViewControllers([arrVC[sender.selectedSegmentIndex]], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)
        }
    }
    
    
    
}
