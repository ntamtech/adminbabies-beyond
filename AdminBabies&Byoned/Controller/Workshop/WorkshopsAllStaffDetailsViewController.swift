//
//  WorkshopsAllStaffDetailsViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/22/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class WorkshopsAllStaffDetailsViewController: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var workShopName: UILabel!
    @IBOutlet weak var workShopDateFrom: UILabel!
    @IBOutlet weak var workShopDateTo: UILabel!
    @IBOutlet weak var workShopTimeFrom: UILabel!
    @IBOutlet weak var workShopTimeTo: UILabel!
    @IBOutlet weak var workShopLocation: UILabel!
    @IBOutlet weak var workShopSpeakerName: UILabel!
    @IBOutlet weak var workShopFee: UILabel!
    @IBOutlet weak var workShopSpeakerBio: UITextView!
    
    // MARK:- Variables
    var WorkshopStaffData : AllStaffWorkshopsData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "WorkShops Information"
        let attributes = [NSAttributedStringKey.font : UIFont(name: "MuseoSans-300", size: 20)!, NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        let button1 = UIBarButtonItem(image: UIImage(named: "leftArrow icon"), style: .plain, target: self, action: #selector((buttonBackNurse)))
        self.navigationItem.leftBarButtonItem  = button1
        
        if let obj = WorkshopStaffData{
            workShopName.text = obj.name
            if let startDate = obj.start_date{
                workShopDateFrom.text = Helper.dateConverterWithFormat(dateString : startDate, isDate: true)
            }
            else{
                workShopDateFrom.text = ""
            }
            if let startTime = obj.start_date{
                workShopTimeFrom.text = Helper.dateConverterWithFormat(dateString : startTime, isDate: false)
            }
            
            else{
                workShopTimeFrom.text = ""
            }
            if let endDate = obj.end_date{
                workShopDateTo.text = Helper.dateConverterWithFormat(dateString : endDate, isDate: true)
            }
            else{
                workShopDateTo.text = ""
            }
            if let endTime = obj.end_date{
                workShopTimeTo.text = Helper.dateConverterWithFormat(dateString : endTime, isDate: false)
            }
            else{
                workShopTimeTo.text = ""
            }
            workShopLocation.text = obj.location
            workShopSpeakerName.text = obj.speaker_name
            workShopSpeakerBio.text = obj.speaker_bio
            workShopFee.text = obj.price
        }
    }

    @objc func buttonBackNurse(sender:UIButton!){
        self.navigationController?.popViewController(animated: true)
    }
    
}
