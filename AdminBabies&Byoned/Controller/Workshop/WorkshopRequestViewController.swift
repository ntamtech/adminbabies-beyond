//
//  WorkshopRequestViewController.swift
//  AdminBabies&Byoned
//
//  Created by esam ahmed eisa on 1/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import STPopup
import ISHPullUp
import ESPullToRefresh
import Toast_Swift


class WorkshopRequestViewController: UIViewController, WorkshopRequestDelegate{
    
    // MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:- Variables
    var navBar:UINavigationController?
    var workshopstaffRequest = [WorkshopData]()
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        // Do any additional setup after loading the view.
        let nib = UINib.init(nibName: "WorkshopRequestTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "WorkshopRequestTableViewCell")
        observeChanges()
        
        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self?.isRefresh = true
                    self?.showWorkshopRequests()
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.tabBarController?.view.makeToast("Check your Internet Connection!")
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }
        
        
    }
    
    
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
   override func viewWillAppear(_ animated: Bool) {
        // Hide the navigation bar for current view controller
        self.navigationController?.isNavigationBarHidden = true
        setUpView()
    }
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(workshopsChanged(_:)), name: NSNotification.Name(rawValue: NotificationType.Workshop.rawValue), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Workshop.rawValue), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    @objc func workshopsChanged(_ notification: NSNotification) {
        guard let action = notification.userInfo?[Constants.Action] as? String else{
            return
        }
        switch action {
        case NotificationAction.CancelAction.rawValue:
            guard let workshopId = notification.userInfo?[Constants.Id] as? String else {
                return
            }
                
            if let i = AccountManager.shared().workshopRespone?.data?.index(where:{$0.id == workshopId}) {
                AccountManager.shared().workshopRespone?.data?.remove(at: i)
                workshopstaffRequest.remove(at: i)
            }
            self.tableView.reloadData()
            break
            case NotificationAction.AddAction.rawValue:
                if let workshopObj = notification.userInfo?[Constants.workshopObj] as? [String:String] {
                    let jsonData = try? JSONSerialization.data(withJSONObject: workshopObj, options: [])
                    var workshopData: WorkshopData?
                    if let data = jsonData {
                        workshopData = try? JSONDecoder().decode(WorkshopData.self, from: data)
                    }
                    AccountManager.shared().workshopRespone?.data?.append(workshopData!)
                    workshopstaffRequest.append(workshopData!)
                    self.tableView.reloadData()
                }
                break
            default: break
        }
    }


    
    func updateUI() {
        if AccountManager.shared().workshopRespone == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
            }
        }
    }
    fileprivate func setUpView() {
        if let workshops = AccountManager.shared().workshopRespone{
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            showWorkshopRequests()
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }

    
    func  ViewWorkshopRequestDetails(btnTag: Int) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WorkshopAcceptViewController") as! WorkshopAcceptViewController
        vc.adminworkshopRequest = workshopstaffRequest[btnTag]
        vc.workshopRequestsViewController = self
        self.navBar?.pushViewController(vc, animated: true)
    }
    
    
    func showWorkshopRequests(){
        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }
        let parameters = ["admin_id":adminID] as [String:Any]
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        AdminServices.AdminworkshopRequest(Constants.AdminWorkShopRequests, params: parameters, completion: { (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false
                    let result = UIScreen.main.bounds.size
                    if result.height <= 568{
                        self.connectionBtn?.setTitle(Constants.FailureAlertiPhone5, for: .normal)
                    }else{
                        self.connectionBtn?.setTitle(Constants.FailureAlert, for: .normal)
                    }
                    
                }else{
                    self.tableView.es.stopPullToRefresh()
                    self.tabBarController?.view.makeToast("Check your Internet Connection!")

                }
            }else{
                // response
                if let result =  response as? WorkshopRequestsResponse {
                    AccountManager.shared().workshopRespone = result
                    self.workshopstaffRequest.removeAll()
                    self.workshopstaffRequest = (AccountManager.shared().workshopRespone?.data)!
                    self.connectionBtn?.isHidden = true

                    self.tableView?.isHidden = false
                    self.isRefresh = false
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()
                }else{
                    return
                }
            }
        })
    }

}

extension WorkshopRequestViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workshopstaffRequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkshopRequestTableViewCell", for: indexPath)  as! WorkshopRequestTableViewCell
        //use tag for button of view details to take data to Assigment screen
        cell.workshopViewDetails.tag = indexPath.row
        
        let obj = workshopstaffRequest[indexPath.row]
        if let clientname = obj.user_name{
            cell.workshopUserName.text = clientname
         }
        
        if let description = obj.name{
            cell.workshopName.text = description
        }
        cell.delegate = self
        return cell
    }
}


