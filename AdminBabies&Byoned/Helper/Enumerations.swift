//
//  Enumerations.swift
//  AdminBabies&Byoned
//
//  Created by NTAM on 2/25/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
enum ESRefreshType {
    case wechat
}
enum NotificationType:String {
    case Workshop = "1"
    case Midwife = "4"
    case Nurse = "5"
    case Babysitter = "6"
}

enum NotificationAction:String {
    case AddAction = "3"
    case CancelAction = "0"
}
