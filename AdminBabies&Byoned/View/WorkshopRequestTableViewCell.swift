//
//  WorkshopRequestTableViewCell.swift
//  AdminBabies&Byoned
//
//  Created by esam ahmed eisa on 1/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class WorkshopRequestTableViewCell: UITableViewCell {
    
    @IBOutlet weak var workshopImage: UIImageView!
    @IBOutlet weak var workshopUserName: UILabel!
    @IBOutlet weak var workshopName: UILabel!
    @IBOutlet weak var workshopViewDetails: UIButton!
    
    var delegate:WorkshopRequestDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        workshopViewDetails.layer.cornerRadius = 3.0
        workshopImage.layer.cornerRadius = workshopImage.frame.size.height/2
        workshopImage.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func showWorkshopRequestViewDetails(_ sender: Any) {
        self.delegate?.ViewWorkshopRequestDetails(btnTag: workshopViewDetails.tag)
    }
    
}

protocol WorkshopRequestDelegate{
    func ViewWorkshopRequestDetails(btnTag: Int)
}
