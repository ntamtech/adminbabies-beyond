//
//  ConnectionButton.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/18/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
import UIKit

//inhrit from uibutton not uiview
class ConnectionButton: UIButton {

//    @objc func buttonAction(sender: UIButton!) {
//        print("Button tapped")
//    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setTitle("Check your Internet Connection!", for: .normal)
        self.setTitleColor(UIColor.gray, for: .normal)
        self.contentHorizontalAlignment = .center
        self.translatesAutoresizingMaskIntoConstraints = false
        self.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        self.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
//        self.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.titleLabel?.lineBreakMode = .byWordWrapping;
        self.titleLabel?.numberOfLines = 0
        self.titleLabel?.textAlignment = .center
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
