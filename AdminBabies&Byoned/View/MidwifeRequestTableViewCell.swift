//
//  MidwifeRequestTableViewCell.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 1/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidwifeRequestTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeDateLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var viewDetailsBtn: UIButton!
    
    var delegate:MidwifeRequestDelegate?
    var babySitterrequest:NurseStaff_request?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewDetailsBtn.layer.cornerRadius = 3.0
        img.layer.cornerRadius = img.frame.size.height/2
        img.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func showMidwifeViewDetails(_ sender: Any) {
        self.delegate?.ViewMidwifeRequestDetails(btnTag: viewDetailsBtn.tag,passedID:(babySitterrequest?.id)!)
    }
    
}

protocol MidwifeRequestDelegate{
    func ViewMidwifeRequestDetails(btnTag:Int,passedID:String)
}
